<?php
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$router->get("/", "AppCheckController@Inspection");

/** device positions */
$router->group(["prefix" => "api/v1/geolocations"], function () use ($router) {
    $router->get("/{device_id}/list", "DevicePositionController@list");
    $router->get("{device_id}/latest", "DevicePositionController@lastPosition");
    $router->post("/", "DevicePositionController@store");
    $router->get("", "DevicePositionController@ambulanceList");
});

/** CRUD DEVICE */
$router->group(["prefix" => "api/v1/device"], function () use ($router) {
    $router->get("/", "DeviceController@list");
    $router->get("/{id}", "DeviceController@show");

    $router->put("/{id}", "DeviceController@update");
    $router->delete("/{id}", "DeviceController@delete");
    $router->post("", "DeviceController@create");
});
