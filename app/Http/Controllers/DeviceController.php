<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DeviceController extends Controller
{
    protected $modelClass = "GpsDevice";
    protected $validation_attributes = [
        'device_id' => 'required|string|max:32',
        'label' => 'required|string|max:40',
        'is_active' => 'boolean',
    ];
    protected $namespace = "\\App\Models\\";

    private function model()
    {
        return $this->namespace . $this->modelClass;
    }
    /**
     * list all model
     */
    function list() {
        $models = $this->model()::paginate(50);
        return response($models);
    }

    /**
     * show single model
     */
    public function show($id)
    {
        return response($this->model()::find($id));
    }
    /**
     * create new model
     */
    public function create(Request $request)
    {
        $this->validate($request, $this->validation_attributes);
        $model = $this->model()::create($request->input());
        return response($mode, 201);
    }
    /**
     * update model
     */
    public function update(Request $request, $id)
    {

        $keys = array_keys($request->input());
        $validation_attributes = array_intersect_key($this->validation_attributes, $keys);
        $this->validate($request, $validation_attributes);
        $model = $this->model()::find($id)->update($request->input());
        return response($model);
    }
    /**
     * delete model
     */
    public function delete(Request $request, $id)
    {
        $this->model()::destroy($id);
        return response(null, 204);
    }
}
