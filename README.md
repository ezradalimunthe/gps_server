# README

This project is simple server as endpoint of gps tracker for arduino project. 

### running deproject for development
 
1. run docker-composer:

```
docker-compose up --build -d

```
2. run container in  interactive mode
```
docker exec -it ezra_gps_server sh
```

3. create .env file
```
vi .env

```
copy-paste config below

```
APP_NAME="MY GPS SERVER"
APP_ENV=local
APP_KEY=0WFh5Gv8VS6Rc4+N6l2KIAzJXDtaGDhVRbH65t4F
APP_DEBUG=true
APP_URL=http://localhost
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=mydatabase
DB_USERNAME=mydatabase_username
DB_PASSWORD=secret

CACHE_DRIVER=file
QUEUE_CONNECTION=sync

```

## Server Endpoints

* Store data to server:

```
POST http://foo.example/api/v1/geolocations/store HTTP/1.1
Host: foo.example
Content-Type: application/x-www-form-urlencoded
Content-Length: 61

device_id=test_device&latitude=3.5951956&longitude=98.6722235
```
example (using arduino / c++):
```
String latitude;
String longitude;

.... latitude=xxxx;
....  longitude=xxxx;
String payload;
payload = "device_id=test_device&latitude="+latitude +"&longitude="+longitude;

String header;
header = "POST http://foo.example/api/v1/geolocations/store HTTP/1.1\r\n"
\+ "Host: foo.example\r\n"
\+ "Content-Type: application/x-www-form-urlencoded\r\n";
\+ "Content-Length: " + payload.length;

String http_cmd;
http_cmd = header + "\r\n\r\n" + payload;

```


