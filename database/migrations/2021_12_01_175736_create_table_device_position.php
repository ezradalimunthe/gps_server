<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDevicePosition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return voidssss
     */
    public function up()
    {
        Schema::create('device_position', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string("device_id", 32);
            $table->point('position')->comment("geo position of device");
            $table->string("label", 40)->nullable()->comment("friendly name of this device");
            $table->string("attached_to")->nullable()->comment("vehicle where the device attached");
            $table->integer("status_id")->default(0)->comment("status of device");
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_position');
    }
}
