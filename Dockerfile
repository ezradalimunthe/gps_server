FROM php:7.3-fpm-alpine

RUN docker-php-ext-install pdo_mysql
COPY ./www.conf /usr/local/etc/php-fpm.d/www.conf

WORKDIR /var/www/html/

# Copy existing application directory permissions

RUN chown -R www-data:www-data /var/www
COPY . .
RUN mkdir /var/www/storage
RUN chmod -R 755 /var/www/storage

RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer
RUN composer install


# WORKDIR /var/www/html/public
EXPOSE 9000
CMD php-fpm
