<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GpsDevice extends Model
{
    protected $fillable = ["device_id", "label", "attached_to", "is_active"];
    protected $table = "gps_device";

}
