<?php
namespace App\WsClient;

class WsClient
{
    public static function send($message)
    {

        $wsserver = env("WEBSOCKET_SERVER", "NOT SET");
        $wsport = env("WEBSOCKET_PORT", "NOT SET");
        $responseMessage = "";
        \Ratchet\Client\connect($wsserver . ':' . $wsport)->then(function ($conn) {
            $conn->on('message', function ($msg) use ($conn) {
                $responseMessage = $msg;
                $conn->close();
            });

            $conn->send($message);
        }, function ($e) {
            $responseMessage = "ERROR: " . $e->getMessage();
        });
        return $responseMessage;
    }
}
