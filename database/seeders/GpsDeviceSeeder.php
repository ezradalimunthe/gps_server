<?php

namespace Database\Seeders;

use App\Models\GpsDevice;
use Illuminate\Database\Seeder;

class GpsDeviceSeeder extends Seeder
{
    /**sss
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $d = new GpsDevice();
        $d->device_id = "pDizCFeWSBxDThec";
        $d->label = "Ambulance 0001";
        $d->attached_to = "Suyono";
        $d->is_active = 1;
        $d->save();

        $d = new GpsDevice();
        $d->device_id = "d_ambulance_2";
        $d->label = "Ambulance 0002";
        $d->attached_to = "Akbar";
        $d->is_active = 1;
        $d->save();
    }
}
