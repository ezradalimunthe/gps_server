<?php

namespace App\Http\Controllers;

use App\Models\DevicePosition;
use DB;
use Illuminate\Http\Request;

class DevicePositionController extends Controller
{
    function list($device_id) {
        $rvalue = DevicePosition::getDeviceCoordinates($device_id);

        return response($rvalue);

    }

    public function lastPosition($device_id)
    {
        $rvalue = DevicePosition::getDeviceLatestCoordinate($device_id);
        return response($rvalue);
    }

    public function ambulanceList()
    {
        $query = "WITH ambulance_latest_post AS "
            . "(select  *, "
            . " rank() over(partition by device_id order by created_at desc) as n "
            . "from device_position) "
            . "select device_id, position, label, attached_to, `status_id`  , created_at from ambulance_latest_post where n = 1";

        $result = DB::select($query);
        $device_position = DevicePosition::hydrate($result);
        return response()->json($device_position);

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'device_id' => 'required|string',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'status_id' => 'numeric',
        ]);

        $model = new DevicePosition;
        $model->device_id = $request->input('device_id');
        $model->latitude = $request->input('latitude');
        $model->longitude = $request->input('longitude');
        $model->status_id = $request->input('status_id');

        $model->save();

        $msg = [
            "type" => "ambulance-status",
            "audiences" => ["monitor"],
            "content" => $model

        ];
        \App\WsClient\WsClient::send($msg);
        return response()->json(["success" => true, "model" => $model]);

    }
}
