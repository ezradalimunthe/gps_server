<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableGpsDevice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gps_device', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string("device_id", 32)->unique();
            $table->string("label", 40)->comment("friendly name of this device");
            $table->string("attached_to")->nullable()->comment("vehicle where the device attached");
            $table->boolean("is_active")->default(1)->comment("flag to set when the device is out of service");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('gps_device');

    }
}
