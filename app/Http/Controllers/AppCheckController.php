<?php

namespace App\Http\Controllers;

class AppCheckController extends Controller
{

    public function Inspection()
    {
        $result = [];
        $result["environment"] = app()->environment();
        $result["App Name"] = $_ENV["APP_NAME"];

        $result["PHP Version"] = phpversion();
        $result["Laravel Version"] = app()->version();
        // Test database connection
        try {
            \DB::connection()->getPdo();
            $result["database_connection"] = ["Test Connection" => "OK"];
        } catch (\Exception $e) {
            //report($e);
            $result["database_connection"] = ["Test Connection" => "NOT OK", "Reason: " => $e->getMessage()];
        }
        // test cache driver
        $cacheDriver = $_ENV["CACHE_DRIVER"];
        $result["Cache Driver"] = $cacheDriver;
        switch ($cacheDriver) {
            case 'memcached':
                # code...
                $memcachedLoaded = extension_loaded("Memcached");
                $result["Memcached Loaded"] = $memcachedLoaded;

                $stats = \Cache::getMemcached()->getStats();
                if ($stats == false) {
                } else {
                    $stats = true;
                }
                $result["Memcached OK"] = $stats;

                break;
            case "file":
                $result["Storage Writeable"] = is_writable(storage_path());

                $fsys = \Cache::getDirectory();
                $result["Cache Directory"] = $fsys;
                break;
            default:
                # code...
                break;
        }
        $result["Cache Prefix"] = \Cache::getPrefix();
        $result["App Debug"] = $_ENV["APP_DEBUG"];
        //$result["loaded Providers"] = app()->loadedProviders;

        return response($result, 200);
    }
}
