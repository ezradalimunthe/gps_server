<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class DevicePosition extends Model
{
    protected $fillable = ["device_id", "position", "status_id", "created_at"];
    protected $table = "device_position";
    protected $hidden = ["position"];
    protected $appends = ["latitude", "longitude", "state", "state_name"];

    private $_latitude;
    private $_longitude;
    public static function boot()
    {
        /**
         * credit to Emilio Borraz https://stackoverflow.com/users/1971514/emilio-borraz
         * src: https://stackoverflow.com/questions/30682538/handling-mysql-spatial-datatypes-in-laravel-eloquent-orm
         */
        parent::boot();
        static::creating(function ($model) {
            if (isset($model->_latitude, $model->_longitude)) {
                $point = $model->_latitude . " " . $model->_longitude;
                $model->setAttribute('position', DB::raw("GeomFromText('POINT(" . $point . ")')"));
            } else {
                //this shouldn't be happened;
                throw \Exception("latitude or longitud is not set.");
            }

            //get device attributes from GpsDevice;

            $gps_device = GpsDevice::where(["device_id" => $model->device_id])->first();
            if ($gps_device) {
                $model->setAttribute("label", $gps_device->label);
                $model->setAttribute("attached_to", $gps_device->attached_to);
            }

        });
        static::updating(function ($model) {
            throw new \Exception('You cannot update this model!');
        });
    }
    public function setLatitudeAttribute($value)
    {
        $this->_latitude = $value;
    }

    public function setLongitudeAttribute($value)
    {
        $this->_longitude = $value;
    }
    public function getLatitudeAttribute()
    {

        if (is_object($this->position) &&
            get_class($this->position) == "Illuminate\Database\Query\Expression") {
            return $this->_latitude;
        };

        if ($this->position == null) {
            return "";
        }
        $coordinates = unpack('x/x/x/x/corder/Ltype/dlat/dlon', $this->position);
        return $coordinates["lat"];

    }

    public function getLongitudeAttribute()
    {
        if (is_object($this->position) &&
            get_class($this->position) == "Illuminate\Database\Query\Expression") {
            return $this->_longitude;
        };
        if ($this->position == null) {
            return "";
        }

        $coordinates = unpack('x/x/x/x/corder/Ltype/dlat/dlon', $this->position);
        return $coordinates["lon"];
    }

    public function getStateAttribute()
    {
        $state = ["idle", "to_scene", "at_scene", "to_hospital"];
        return $state[$this->status_id];
    }

    public function getStateNameAttribute()
    {
        $state = ["Idle", "Heading to case scene", "Arrived at scene", "Heading to hospital"];
        return $state[$this->status_id];
    }

    public static function GetDeviceCoordinates($device_id)
    {
        $result = DevicePosition::orderBy("created_at", "desc")
            ->where(["device_id" => $device_id])->paginate(20);
        return $result;
    }

    //TODO: TEST THIS FUNCTION!
    public static function GetDeviceCoordinatesBetweenDates($device_id, $startdate, $enddate)
    {
        $result = DevicePosition::orderBy("created_at", "desc")
            ->where(["device_id" => $device_id])
            ->whereBetween("created_at", [$startdate, $enddate])
            ->paginate(20);
        return $result;
    }

    public static function GetDeviceLatestCoordinate($device_id)
    {

        $results = DevicePosition::where(["device_id" => $device_id])
            ->orderBy('created_at', 'desc')->first();
        return $results;
    }

    public static function all($column = [])
    {
        throw new \Exception('DO NOT call all() method for this model!');
    }
}
